import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'

const Status = ({ data }) => {
    return (
        <View style={styles.containerStatus}>
            <Image source={require('../img/bitcoin.png')} style={styles.tinyLogo}/>
            <Text style={styles.text}>{data}</Text>
        </View>
    )
}

export default Status

const styles = StyleSheet.create({
    text: {
        color: 'white',
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'right',
        width: 70,
    },
    tinyLogo: {
        width: 20,
        height: 20,
        margin: 2,
    },
    containerStatus: {
        flexDirection: 'row',
        justifyContent: 'flex-arround',
        alignItems: 'center',
        marginTop: 10,
        marginRight: 10,
        backgroundColor: '#000000a0',
        width: 100,
        borderRadius: 10,
        padding: 2,
    }
})