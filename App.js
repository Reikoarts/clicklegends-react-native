import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ImageBackground, TouchableHighlight, Dimensions, Button } from 'react-native';
import Gif from 'react-native-gif'
import { useState, useEffect } from 'react';
import Status from './assets/componants/Status';

export default function App() {

  const [money, setMoney] = useState(0)
  const [moneyPerClick, setMoneyPerClick] = useState(1)
  const [autoMoney, setAutoMoney] = useState(3)

  useEffect(() => {
    const intervalId = setInterval(() => {
      setMoney((prevMoney) => prevMoney + autoMoney);
    }, 1000);

    return () => clearInterval(intervalId); // Cleanup the interval on unmount
  }, [autoMoney]);

  return (
    <View style={styles.container}>
      <TouchableHighlight
        activeOpacity={0.95}
        underlayColor="#DDDDDD"
        onPress={() => setMoney(money + 1)}>
        <ImageBackground source={require('./assets/img/background.jpeg')} resizeMode="cover" style={styles.image}>
          <Gif source={require('./assets/animation/gameplay.gif')} style={[{ width: 800, height: 800 }, styles.gif]} />

          <Status data={money} />
          <Status data={moneyPerClick} />
          <Status data={autoMoney} />

        </ImageBackground>
      </TouchableHighlight>
    </View>
  );
}

windowWidth = Dimensions.get('window').width;
windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: windowWidth,
    height: windowHeight,
    position: 'relative',
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    objectFit: 'cover',
    width: windowWidth,
    height: windowHeight,
  },
  gif: {
    position: 'absolute',
    bottom: 0,
    left: -200,
    right: 0,
    top: 230,
    zIndex: 2
  },
  button: {
    zIndex: 4
  }
});
